

/**
 * C application that accepts filename from the command line and 
 * creates copy of file with all contents reversed.
 */

#include<stdio.h>

int main(int argc, char* argv []) {
    
    
    int pos, i;
    char ch;
    
    // open file in read mode
    FILE *fp = fopen(argv[1], "r");
    FILE *fp2;
    if (argv[2] == NULL) {
        // open file in write mode or create file
        fp2 = fopen("outputData.txt", "w+");
    } else {
        // open file in write mode or create file
        fp2 = fopen(argv[2], "w+");
    } 
    
    // check whether file exist or not
    if( fp == NULL )
    {
        printf("File does not exist..");
        return 0;
    }
    fseek(fp,0,SEEK_END);
    pos=ftell(fp);
    i = 0;
    
    while(i<pos) {
        i++;
        fseek(fp,-i,SEEK_END);
        ch=fgetc(fp);
        fputc(ch, fp2);
    }
      
    fclose(fp);
    fclose(fp2);
    
    return 0;
}